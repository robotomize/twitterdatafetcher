#!/bin/python3
# -*- coding: utf-8 -*-

import json

class JsonUtils:

    def __init__(self):
        pass

    def json_dic(self, source_string):
        return json.loads(source_string)

    def json_filter(self, source_string, params = 0):
        try:
            if params == 0:
                return self.json_dic(source_string)
            else:
                return {x:json.loads(source_string)[x] for x in params}
        except BaseException as e:
            print(str(e))


