#!/bin/python3
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

__author__ = 'robotomize'

from abc import ABCMeta, abstractmethod, abstractproperty

class AbstractFactory():

    @abstractmethod
    def create(self): pass

    @abstractmethod
    def delete(self, object_name): pass

