#!/bin/python3
# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function
import sys
sys.path.append('../utils')
sys.path.append('../fetchers')
sys.path.append('../generators')
sys.path.append('../accounters')
import os
import time
import random
import asyncio
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener

import generate_movie
import abstract_listener
import json_file_fetcher
import mongo_fetcher
import json_utils

__author__ = 'robotomize'

class TwitterListener(abstract_listener.AbstractListener, StreamListener):
	# максимальный размер подписок на события в твиттере с одного акка, нужнов ынести из реализации
	max_subscribers = 390

	def __init__(self, start_time, time_limit=60):
		self.time = start_time
		self.limit = time_limit
		self.state = 'stop'
		self.listen('started')

	def listen(self, status):
		self.state = status

	def get_state(self):
		return self.state

	def on_data(self, data):
		fetch_obj = mongo_fetcher.MongoFetcher('localhost', 'twitter_data', 'movies_data', 27017)
		fetch_obj.connect()

		while (time.time() - self.time) < self.limit:
			try:
				self.listen('Fetching')
				fetch_obj.pull(data, ['text', 'created_at'])
				fetch_obj.commit()
				return True

			except BaseException as e:
				print('Failed on data', str(e))
				self.listen('wait')
				time.sleep(1)
				pass

		exit(1)

	def on_error(self, status):
		print(status)
