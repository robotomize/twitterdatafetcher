#!/bin/python3
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

__author__ = 'robotomize'

from abc import ABCMeta, abstractmethod, abstractproperty


class AbstractListener():

    __metaclass__=ABCMeta

    @abstractmethod
    def listen(self, status): pass

    @abstractmethod
    def get_state(self): pass

    @abstractmethod
    def set_state(self): pass