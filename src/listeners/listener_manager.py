#!/bin/python3
# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function
import sys
import time
import os
import time
import random
sys.path.append('../')
sys.path.append('../accounters')
sys.path.append('../generators')
import configparser
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener
import abstract_manager
import account_factory
import generate_movie
import twitter_listener

'''
	# asyncio library usage

	#loop = asyncio.get_event_loop()
	#loop.call_soon(twitterStream, loop)
	#loop.call_soon(hello_world, loop)
	# Blocking call interrupted by loop.stop()
	#loop.run_forever()
	#loop.close()

     Неприоритетно, но прикольно замутить аткую штуку
     @TODO сделать скедулер аккаунтеров или лисенеров, чтобы видеть все объекты и их статусы
     @TODO сделать ботов в телеграм, для отслеживания состояния процессов фетчинга данных
     @TODO хочу сделать некий абстрактный класс для мониторинга событий в проекте
     Пусть будет агрегатор агрегатор событий базовый класс и агрегатор эксепшнов дочерний(агрегатор экспшнов не должен копить
     все эксепшны, он должен накаплдивать и группировать их),
     чтобы лисенеры слушая агрегаторы принимали нормальный поток
     Также сделать базовый лиссенер событий и базовый сендер событий
     Дочерний лисенер эксепшн событий должен создавать дочерний объект сендера собтиый(например телеграмма или email) для отправки событий

'''

class ListenerManager(abstract_manager.AbstractManager):

    def __init__(self):
        self.object_count = 0

    objects_matrix = []

    account_keys = [{
                    'ckey' : 'O3vBgvb0Rdi491Jiv0sbw3Ent',
                    'cons_secret' : '5bjf5lbsSy6ypl4A7e6IWv6ogfDpZXDwUpn2f7YrUUplR0nzif',
                    'access_tok_key' : '2156739297-O5zVmLWDGQdf5mPyneUIQXMCG4C3a3wgbzZLeyG',
                    'access_tok_sec' : '6FYkUdR9SIu9HtEJWj7OzVZSgi3kkNJ7SxUQzAYws2rRG'
                    }]

    def init_manager(self, object = []):
        print('Ok, listen manager started')
        self.movie_list_obj = generate_movie.GenerateMovieList()
        self.keyword_list = self.movie_list_obj.generate('/Users/robotomize/Documents/prepared_list_2')
        self.create_listener()

    def create_listener(self):
        try:
            self.twitter_account = account_factory.AccountFactory.create(self, method = 'create_twitter', **self.account_keys[0])
            if self.twitter_account is not None:
                print('Object', 'create_twitter', 'created')

                start_time = time.time()
                twitterStream = Stream(self.twitter_account, twitter_listener.TwitterListener(start_time, time_limit=60))

                '''
                @TODO вынести генерирование рендомных диапазонов в utils
                '''
                min_a = random.randrange(0,1120570)
                max_b = random.randrange(0,1120570)

                while min_a - max_b > twitter_listener.TwitterListener.max_subscribers or max_b - min_a > twitter_listener.TwitterListener.max_subscribers:
                    min_a = random.randrange(0,1120570)
                    max_b = random.randrange(0,1120570)
                if min_a > max_b:
                    tmp = max_b
                    max_b = min_a
                    min_a = tmp

                print(self.keyword_list[min_a:max_b])

                print(str(min_a) + '\n')
                print(str(max_b) + '\n')

                twitterStream.filter(track=self.keyword_list[0:twitter_listener.TwitterListener.max_subscribers], languages=['en'], async=True)
                self.object_set_count()
                self.set_objects_matrix(twitterStream)
                print(self.get_objects_matrix())
        except BaseException as e:
            print(str(e))

    def get_object_status(self, object_name):
        pass

    def object_set_count(self):
        self.object_count += 1

    def object_get_count(self):
        return self.object_count

    def set_objects_matrix(self, object):
        self.objects_matrix.append(object)

    def get_objects_matrix(self):
        return self.objects_matrix
