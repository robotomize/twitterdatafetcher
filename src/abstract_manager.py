#!/bin/python3
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

__author__ = 'robotomize'

from abc import ABCMeta, abstractmethod, abstractproperty

class AbstractManager():

    @abstractmethod
    def init_manager(self): pass

    @abstractmethod
    def get_object_status(self, object_name): pass

    @abstractmethod
    def object_get_count(self): pass

    @abstractmethod
    def object_set_count(self): pass

    @abstractmethod
    def get_objects_matrix(self): pass

    @abstractmethod
    def set_objects_matrix(self): pass