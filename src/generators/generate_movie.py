#!/bin/python3
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import io
import re
import codecs
import abstract_generate_list

# Генератор списка кей вордов фильмов. Работает на базе дампов IMDB
# Генерирует список тегов(названий фильмов)

__author__ = 'robotomize'


class GenerateMovieList(abstract_generate_list.AbstractGenerateObjectList):

    def __init__(self):
        self._movie_tag_list = []

    def fetch(self, full_path_filename):
        try:
            file_res = io.open(full_path_filename, encoding='utf-8', errors='ignore')
            for line in file_res:
                convert_srt =  line.strip()
                if (convert_srt != '' or convert_srt != ' ' ):
                    self._movie_tag_list.append(convert_srt)
            file_res.close()

        except BaseException as e:
            print('Failed fetch data from file', str(e))


    def generate(self, full_path_filename):
        self.fetch(full_path_filename)
        return [value for value in self._movie_tag_list if value]

