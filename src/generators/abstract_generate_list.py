#!/bin/python3
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Генератор списка объектов-тегов, нужен чтобы создать список кей вордов объектов
__author__ = 'robotomize'

from abc import ABCMeta, abstractmethod, abstractproperty


class AbstractGenerateObjectList():

    __metaclass__=ABCMeta

    @abstractmethod
    def fetch(self, file_name): pass

    @abstractmethod
    def generate(self, file_name): pass





