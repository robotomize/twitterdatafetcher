#!/bin/python3
# -*- coding: utf-8 -*-
'''
from __future__ import unicode_literals

import asyncio

def hello_world(loop):
    print('Hello World')
    #loop.stop()

loop = asyncio.get_event_loop()

# Schedule a call to hello_world()
loop.call_soon(hello_world, loop)
loop.call_soon(hello_world, loop)
# Blocking call interrupted by loop.stop()
loop.run_forever()
loop.close()
'''

class Subject(object):

    def __init__(self):
        self.events = []

    def register(self, observer):
        # Adding the observer for the event
        self.events.append(observer)

    def unregister(self, observer, event):
        self.events.remove(observer)

    def notify(self, event):
        for observer in self.events:
            observer.notify(event)




class Observer(object):

    def __init__(self, name):
        self.name = name

    def notify(self, event):
        print '{name} executing {event}'.format(name=self.name, event=event)


if __name__ == '__main__':
    import doctest
    doctest.testmod()

subject = Subject()
observer1 = Observer('observer1')
observer2 = Observer('observer2')
subject.register(observer1)
subject.register(observer2)
subject.notify('event1')
subject.notify('event56')