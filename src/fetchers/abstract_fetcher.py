#!/bin/python3
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

__author__ = 'robotomize'

from abc import ABCMeta, abstractmethod, abstractproperty


class AbstractFetcher():

    __metaclass__=ABCMeta

    @abstractmethod
    def put(self, attrs): pass

    @abstractmethod
    def commit(self): pass

    @abstractmethod
    def pull(self, data, params): pass