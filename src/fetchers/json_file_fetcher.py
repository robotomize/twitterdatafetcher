#!/bin/python3
# -*- coding: utf-8 -*-
import sys
sys.path.append('../utils')
import json
import abstract_fetcher
import json_utils
class JsonFileFetcher(abstract_fetcher.AbstractFetcher):
    def __init__(self, file_name):
        self.file_name = file_name
        pass

    def put(self, id):
        pass

    def pull(self, data, params = 0):
        if data is not None:
            json_pull = json_utils.JsonUtils()
            self.data = json_pull.json_filter(data, params)

    def commit(self):
        try:
            commit_to_file = open(self.file_name, 'a')
            commit_to_file.write(self.data)
            commit_to_file.write('\n')
            commit_to_file.close()
        except BaseException as e:
            print(str(e))



