#!/bin/python3
# -*- coding: utf-8 -*-
from pymongo import MongoClient
import sys
sys.path.append('../utils')
import json
import abstract_fetcher
import json_utils

class MongoFetcher(abstract_fetcher.AbstractFetcher):
    def __init__(self, host, db_name, collection_name, port):
        self.db = db_name
        self.collection_name = collection_name
        self.host = host
        self.port = port
    def connect(self):
        try:
            self.client = MongoClient(self.host, self.port)
            self.dbb = self.client[self.db]
            self.collection = self.dbb[self.collection_name]
        except BaseException as e:
            print(str(e))

    def pull(self, data, params = 0):
        if data is not None:
            json_pull = json_utils.JsonUtils()
            self.data = json_pull.json_filter(data, params)
        else:
            pass

    def put(self, attrs):
        pass

    def commit(self):
        if self.data is not None:
            try:
                self.collection.insert(self.data)
            except BaseException as e:
                print(str(e))

    def put_all(self):
        for data in self.dbb.collection_name.find():
            print(data)


