#!/bin/python3
# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function
import sys
sys.path.append('../')
import configparser
from tweepy import OAuthHandler
import abstract_manager
import account_factory

class AccountManager(abstract_manager.AbstractManager):

    account_keys = [{
                    'ckey' : 'O3vBgvb0Rdi491Jiv0sbw3Ent',
                    'cons_secret' : '5bjf5lbsSy6ypl4A7e6IWv6ogfDpZXDwUpn2f7YrUUplR0nzif',
                    'access_tok_key' : '2156739297-O5zVmLWDGQdf5mPyneUIQXMCG4C3a3wgbzZLeyG',
                    'access_tok_sec' : '6FYkUdR9SIu9HtEJWj7OzVZSgi3kkNJ7SxUQzAYws2rRG'
                    }]

    account_provider = {
                        'twitter'      : 'create_twitter',
                        'twitter_gr'   : 'create_group_twitter',
                        'instagram'    : 'create_instagram',
                        'instagram_gr' : 'create_group_instagram',
                        'vk'           : 'create_vk',
                        'vk_gr'        : 'create_group_vk',
                        'fb'           : 'create_fb',
                        'fb_gr'        : 'create_group_fb',
                        'google'       : 'create_google',
                        'google_gr'    : 'create_group_google'
                    }

    objects_matrix = []

    def __init__(self):
        self.object_count = 0

    def account_create(self):
        # test okay?
        try:
            self.twitter_account = account_factory.AccountFactory.create(self, method = self.account_provider['twitter'], **self.account_keys[0])
            if self.twitter_account is not None:
                print('Object', self.account_provider['twitter'], 'created')
                self.object_set_count()
                self.set_objects_matrix(self.twitter_account)
        except BaseException as e:
            print(str(e))

    def init_manager(self):
        print('Ok, account manager started..')
        self.account_create()

    def get_object_status(self, object_name):
        pass

    def object_set_count(self):
        self.object_count += 1

    def object_get_count(self):
        return self.object_count

    def set_objects_matrix(self, object):
        self.objects_matrix.append(object)

    def get_objects_matrix(self):
        return self.objects_matrix
