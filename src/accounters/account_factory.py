#!/bin/python3
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import abstract_account_factory
__author__ = 'robotomize'
import twitter_accounter

class AccountFactory(abstract_account_factory.AbstractFactoryAccount):

    @staticmethod
    def create(self, method = 'create_twitter',  **kwargs):
        try:
            return getattr(AccountFactory, method)(self, **kwargs)
        except BaseException as e:
            print('call undefined function, error code this ', str(e))

    @staticmethod
    def delete(self):
        pass

    @staticmethod
    def create_twitter(self, **kwargs):
        return twitter_accounter.TwitterAccounter(ckey=kwargs['ckey'], cons_secret = kwargs['cons_secret'],
                access_tok_key = kwargs['access_tok_key'], access_tok_sec = kwargs['access_tok_sec']).create()

    def create_group_twitter(self): pass

    def create_instagram(self, **kwargs): pass

    def create_group_instagram(self): pass

    def create_vk(self, **kwargs): pass

    def create_group_vk(self): pass

    def create_fb(self, **kwargs): pass

    def create_group_fb(self): pass

    def create_google(self, **kwargs): pass

    def create_group_google(self): pass