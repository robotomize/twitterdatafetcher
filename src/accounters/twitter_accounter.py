#!/bin/python3
# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function
import sys
import configparser
from tweepy import OAuthHandler
import abstract_accounter

class TwitterAccounter(abstract_accounter.AbstractAccounter):

    provider_name = 'twitter'

    def __init__(self, ckey = '', cons_secret = '', access_tok_key = '', access_tok_sec = ''):
        self.c_key = ckey
        self.cons_sec = cons_secret
        self.acc_tok_key = access_tok_key
        self.acc_tok_sec = access_tok_sec
        # Тут лежат ключи в порядке использования в объектах
        self.sec_list = {}
    def create(self):
        auth = OAuthHandler(self.c_key, self.cons_sec)
        auth.set_access_token(self.acc_tok_key, self.acc_tok_sec)
        return auth

    def load_conf(self):
        config = configparser.ConfigParser()
        config.read('configs/social_accounts.conf')
        arg_list = []
        for sec in config.sections():
            for arg in config[sec]:
                arg_list.append(config[sec][arg])
            self.sec_list[sec] = arg_list
            arg_list = []

    def write_conf(self):
        pass
