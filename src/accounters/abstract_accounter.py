#!/bin/python3
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

__author__ = 'robotomize'

from abc import ABCMeta, abstractmethod, abstractproperty

class AbstractAccounter():

    __metaclass__=ABCMeta

    provider_name = ''

    @abstractmethod
    def create(self): pass

    @abstractmethod
    def load_conf(self): pass

    @abstractmethod
    def write_conf(self): pass