#!/bin/python3
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import sys
sys.path.append('../')
__author__ = 'robotomize'

from abc import ABCMeta, abstractmethod, abstractproperty
import abstract_factory

class AbstractFactoryAccount(abstract_factory.AbstractFactory):

    @abstractmethod
    def create(self): pass

    @abstractmethod
    def delete(self): pass

    @abstractmethod
    def create_twitter(self, **kwargs): pass

    @abstractmethod
    def create_group_twitter(self): pass

    @abstractmethod
    def create_instagram(self, **kwargs): pass

    @abstractmethod
    def create_group_instagram(self): pass

    @abstractmethod
    def create_vk(self, **kwargs): pass

    @abstractmethod
    def create_group_vk(self): pass

    @abstractmethod
    def create_fb(self, **kwargs): pass

    @abstractmethod
    def create_group_fb(self): pass

    @abstractmethod
    def create_google(self, **kwargs): pass

    @abstractmethod
    def create_group_google(self): pass